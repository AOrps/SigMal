# Malware Development

## Purpose
- Learn how to make "Malware" from Scratch for the intended purpose of learning how it can be developed and then later analyzed.

- Purely educational PROOF OF CONCEPT


## Goal
If 1 Team:
Team a piece of custom malware executable ("ransomware") 


If 2 Teams:
Teams create a custom malware executable ("ransomware") and compete against each other using different languages and potentially in challenges. 

## Steps
<!-- 
Jobs:
-> Open CMD / Terminal 
-> Work with Executable Arguments 
-> Work with Executable I/O
-> Work with Files
-> Encrypt files
-> Decrypt files
-> Compile to Different OS + Arch
-> Integrate all code
-->
1. Open CMD / Terminal 
1. Run executable with some start argument
1. Encrypting Files in some directory
1. Decrypt Files after putting in the Password
1. Compile to Different Operating Systems + Architectures

## Bonus Steps
- Create Service / Process Daemon
- Checks for Antivirus Software
    - And Kills Processes
- Evades Detection 
- Packs Binary  (UPX)


## Resources
- [Binjection](https://github.com/Binject/binjection)
- [Universal](https://github.com/Binject/universal)
- [Golang Encryption / Decryption with AES](https://tutorialedge.net/golang/go-encrypt-decrypt-aes-tutorial/)


## Sig•Mal Projects Groups
| Group #1                      | Group 2
|:---                           |:----
|                               | 