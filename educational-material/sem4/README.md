# Semester 4

## Game Plan

|Icon                       | Title                                          | Description
| :---:                     | :--:                                           | :----
| :new:                     | Intro                                          | Classic Intro
| [:round_pushpin:](02.md)  | [Graphs, Graphs, Graphs](02.md)                | Graphs, Control Flow Graphs, Visualizing Malware as a Graph
| [:lock:](03.md)           | [Finally Some Crypto](03.md)                   | Hashing, Encryption,  Argon2
| [:page_facing_up:](04.md) | [Sigmal's "TPS Report" /w Cover Sheets](04.md) | Hatching Triage and TTP Report (MITRE ATT&CK)   
| [:books:](05.md)          | [Malware Primer](05.md)                        | Info Necessary for Writing our own Malware
| [:shipit:](06.md)         | [0x`m4l-d3v`](06.md)                           | Writing our own Malware
| [:dragon_face:](07.md)    | [NSA's Ghidra](07.md)                          | Reversing Malware with Ghidra 
<!-- | [:fox_face:](08.md)       | [BishopFox's Sliver](08.md)                    | Learning an Adversary Emulation Framework  -->

<!-- :globe_with_meridians: or :round_pushpin: for graphs -->

## Constraints
- Slides can't have more than 5 sub-topics.
- Attempt to have some concrete example (code, demo) along with lesson.
- Ensure all images, content is licensed with Creative Commons or Open-Sourced
- Resize Images to (height=300, width=auto) (check comment here)
<!-- TEMPLATE FOR Resized Image (scales down to height=300): 
<p align="center">
    <img 

    src="https://raw.githubusercontent.com/AOrps/SigMal/main/educational-material/4sem/img/{image-name}.png"

    alt="" 
    
    data-canonical-src="{Enter Creative Commons picture if not generated from scratch}" 
    
    height="300" width="auto"
    
    >
</p>

\\ technically don't need the width="auto" but we ballin'
-->

